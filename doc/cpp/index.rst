.. highlight:: cpp

C++ API
=======

.. toctree::
   :maxdepth: 1

   formula/index.rst
   types/index.rst
   formula_name_resolver/index.rst
   model_context/index.rst
   formula_cell/index.rst
   interface/index.rst


